import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

/// Returns the app working directory for various platforms
Future<Directory> getAppStorageDirectory() async {
  Directory? appDocDirectory;
  if (Platform.isAndroid) {
    appDocDirectory = await getExternalStorageDirectory();
  } else if (Platform.isIOS) {
    appDocDirectory = await getApplicationDocumentsDirectory();
  } else {
    appDocDirectory = await getTemporaryDirectory();
  }

  if (appDocDirectory == null) throw("Platform not supported.");

  return appDocDirectory;
}

/// Copies a given file to the working directory for various platforms
Future<File> copyFileToApplicationDocumentDirectory(String imagePath) async {
  final Directory? appDocDirectory = await getAppStorageDirectory();

  final name = basename(imagePath);
  final image = File('${appDocDirectory!.path}/$name');

  return File(imagePath).copy(image.path);
}

/// Loads a file as an ui.Image
Future<ui.Image> loadImage(File file) async {
  final data = await file.readAsBytes();
  return await decodeImageFromList(data);
}

/// Saves an encoded image to a file
Future<File> saveImageToFile(Uint8List image, String name) async {
  final dir = await getAppStorageDirectory();
  final imagePath = dir.path + "/$name.png";
  File imageFile = File(imagePath);

  // For now we just overwrite the existing files
  // Not needed: if (await imageFile.exists()) {
  imageFile.create(recursive: true);
  // TODO: Cleanup files after exiting

  imageFile.writeAsBytes(image);
  return imageFile;
}

/// Triggers the ImagePicker as a Dialog overlay
Future pickImage(Function(File) callback) async {
  try {
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (image == null) return;
    callback(File(image.path));
  } on PlatformException catch (e) {
    // Note: Don't pick the first image on the iOS simulator ;)
    // s. https://github.com/flutter/flutter/issues/82602#issuecomment-858383514
    print('Failed to pick logo image: $e');
  }
}

/// Converts a byte list into an Image
Future<Image> bytesToImageWidget(Uint8List bytes) async {
  return Image.memory(bytes);
}

/// Converts a byte list into an ui.Image
Future<ui.Image> bytesToImage(Uint8List imgBytes) async {
  ui.Codec codec = await ui.instantiateImageCodec(imgBytes);
  ui.FrameInfo frame = await codec.getNextFrame();
  return frame.image;
}

/// Calculates the foreground color according to the w3c standard
Color getFontColorForBackground(Color background) {
  return (background.computeLuminance() > 0.179) ? Colors.black : Colors.white;
}

// Based on helpers from https://stackoverflow.com/a/63299945/3041394

/// Relatively change a colors saturation
Color increaseColorSaturation(Color color, double increment) {
  var hslColor = HSLColor.fromColor(color);
  var newValue = min(max(hslColor.saturation + increment, 0.0), 1.0);
  return hslColor.withSaturation(newValue).toColor();
}

/// Relatively change a colors lightness
Color increaseColorLightness(Color color, double increment) {
  var hslColor = HSLColor.fromColor(color);
  var newValue = min(max(hslColor.lightness + increment, 0.0), 1.0);
  return hslColor.withLightness(newValue).toColor();
}

/// Relatively change a colors hue
Color increaseColorHue(Color color, double increment) {
  var hslColor = HSLColor.fromColor(color);
  var newValue = min(max(hslColor.hue + increment, 0.0), 360.0);
  return hslColor.withHue(newValue).toColor();
}

/// Navigate through pages without the slide-from-the-side animation
void navigateToViewWithoutTransition(BuildContext context, Widget view) {
  Navigator.pushReplacement(
    context,
    PageRouteBuilder(
      pageBuilder: (context, animation1, animation2) => view,
      transitionDuration: Duration.zero,
      reverseTransitionDuration: Duration.zero,
    ),
  );
}
