import 'package:cv_image_editor/views/about_view.dart';
import 'package:cv_image_editor/views/create_screenshots_view.dart';
import 'package:cv_image_editor/views/process_screenshots_view.dart';
import 'package:cv_image_editor/views/show_images_result_view.dart';
import 'package:cv_image_editor/views/create_images_view.dart';
import 'package:cv_image_editor/views/process_image_view.dart';
import 'package:cv_image_editor/views/show_screenshots_result_view.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const CVImageEditorApp());
}

class CVImageEditorApp extends StatelessWidget {
  const CVImageEditorApp({Key? key}) : super(key: key);

  static const String _title = 'Awesome Thumbnail Composer';

  // Application root
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.purpleAccent,
        buttonTheme: const ButtonThemeData(
          buttonColor: Colors.deepPurple,
          textTheme: ButtonTextTheme.primary,
        ),
        textTheme: const TextTheme(
          headline2: TextStyle(color: Colors.purpleAccent),
          headline6: TextStyle(color: Colors.grey)
        ),
        // Define the default font family.
        fontFamily: 'IBMPlexMono',
      ),
      initialRoute: CreateImagesView.route,
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        CreateImagesView.route: (context) => const CreateImagesView(),
        CreateScreenshotsView.route: (context) => const CreateScreenshotsView(),
        ProcessImagesView.route: (context) => const ProcessImagesView(),
        ProcessScreenshotsView.route: (context) => const ProcessScreenshotsView(),
        ShowImagesResultView.route: (context) => const ShowImagesResultView(),
        ShowScreenshotsResultView.route: (context) => const ShowScreenshotsResultView(),
        AboutView.route: (context) => const AboutView(),
      },
    );
  }
}