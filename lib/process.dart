/// The arguments passed from create_images_view to process_image_view
class ProcessImagesArguments {
  final String name;
  final String id;
  final String color;
  final bool useGradient;
  final String appIconPath;
  final String textLogoImagePath;

  ProcessImagesArguments({
    required this.name,
    required this.id,
    required this.color,
    required this.useGradient,
    required this.appIconPath,
    required this.textLogoImagePath,
  });
}

/// The arguments passed from create_screenshot_view to process_screenshot_view
class ProcessScreenshotsArguments {
  final String name;
  final String id;
  final String color;
  final List<String> osTypes;
  final List<String> types;
  final bool useGradient;
  final List<String> screenshotPaths;

  ProcessScreenshotsArguments({
    required this.name,
    required this.id,
    required this.color,
    required this.osTypes,
    required this.types,
    required this.useGradient,
    required this.screenshotPaths,
  });
}

// TODO: Check how to pass named parameter to super constructor
// using class ProcessResultArguments extends ProcessArguments
// and B({min: 2, max, step: 1}) : super(min: min, max: max);
// e.g. Bar({a,b}) : super(a:a, b:b)
/// The arguments passed from process_image_view to show_screenshots_image_view
class ProcessImageResultArguments {
  final String name;
  final String id;
  final String appIconPath;
  final String textLogoImagePath;
  final String androidAppIcon;
  final String androidFeatureImage;
  final String androidTvBanner;
  final String itchCover;
  final String itchBanner;
  final String itchFavicon;
  final String itchWideCover;
  final String itchSocial;
  final String itchLogo;

  ProcessImageResultArguments({
    required this.name,
    required this.id,
    required this.appIconPath,
    required this.textLogoImagePath,
    required this.androidAppIcon,
    required this.androidFeatureImage,
    required this.androidTvBanner,
    required this.itchCover,
    required this.itchBanner,
    required this.itchFavicon,
    required this.itchWideCover,
    required this.itchSocial,
    required this.itchLogo,
  });
}

/// The arguments passed from process_screenshot_view to show_screenshots_result_view
class ProcessScreenshotResultArguments {
  final String name;
  final String id;
  final List<String> osTypes;
  final List<String> types;
  final List<String> screenshotPaths;

  ProcessScreenshotResultArguments({
    required this.name,
    required this.id,
    required this.osTypes,
    required this.types,
    required this.screenshotPaths,
  });
}

/// Defines the available image types
enum ImageType {
  // Android
  androidAppIcon,
  androidFeatureImage, // Vorstellungsgrafik
  androidTvBanner,

  // ITCH.IO
  itchCover, // 630x500
  // Metadata/Promo images
  itchSocial, // FB sharing
  itchFavicon, // for browser, square
  itchWideCover, // 21:9
  itchLogo, // the text logo as full width image
  // Game page theme
  itchBanner, // 960x400
  //itchEmbedBG,
  //itchBackground,
}

/// Defines the available screenshot operation system types
enum ScreenshotOSType {
  // Android
  android,
  // iOS
  ios,
}

/// Return the OS type for a given string
ScreenshotOSType getOsTypeForString(String osTypeString) {
  return (osTypeString == ScreenshotOSType.ios.name) ? ScreenshotOSType.ios : ScreenshotOSType.android;
}

/// Defines the available screenshot types
enum ScreenshotType {
  rescaled,
  promo,
}

/// Return the screenshot type for a given string
ScreenshotType getTypeForString(String osTypeString) {
  return (osTypeString == ScreenshotType.promo.name) ? ScreenshotType.promo : ScreenshotType.rescaled;
}

/// Define the available screen size types
enum ScreenshotSize {
  androidPhone,
  android7Tablet,
  android10Tablet,
  iPhone11ProMax,
  iPhone11Pro,
  iPhone6Plus,
  iPhone6,
  iPadPro3,
  itchIo,
}