import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/process.dart';
import 'package:cv_image_editor/views/show_screenshots_result_view.dart';
import 'package:flutter/material.dart';

class ProcessScreenshotsView extends StatefulWidget {
  const ProcessScreenshotsView({Key? key}) : super(key: key);

  static const String route = '/processScreenshots';

  @override
  _ProcessScreenshotsViewState createState() => _ProcessScreenshotsViewState();
}

class _ProcessScreenshotsViewState extends State<ProcessScreenshotsView> {
  Future<void> createImages(ProcessScreenshotsArguments args) async {
    // Aggregate all the files for the provided screenshots
    List<File> files = [];
    for (var i = 0; i < args.screenshotPaths.length; i++) {
      files.addAll(await createScreenshots(args, args.screenshotPaths[i], i));
    }

    Navigator.popAndPushNamed(
      context,
      ShowScreenshotsResultView.route,
      arguments: ProcessScreenshotResultArguments(
          name: args.name,
          id: args.id,
          osTypes: args.osTypes,
          types: args.types,
          screenshotPaths: files.map((f) => f.path).toList()),
    );
  }

  /// Create all the processed screenshots according to the properties specified
  /// in args and the given screenshot.
  Future<List<File>> createScreenshots(ProcessScreenshotsArguments args, String path, int id) async {
    var screenshot = await loadImage(File(path));
    List<File> generatedScreenshots = [];
    var name = args.name + "_" + id.toString();

    if (args.osTypes.contains(ScreenshotOSType.ios.name)) {
      for (var type in args.types) {
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.ios, getTypeForString(type), ScreenshotSize.iPhone11ProMax));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.ios, getTypeForString(type), ScreenshotSize.iPhone11Pro));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.ios, getTypeForString(type), ScreenshotSize.iPhone6Plus));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.ios, getTypeForString(type), ScreenshotSize.iPhone6));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.ios, getTypeForString(type), ScreenshotSize.iPadPro3));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.ios, getTypeForString(type), ScreenshotSize.itchIo));
      }
    }

    if (args.osTypes.contains(ScreenshotOSType.android.name)) {
      for (var type in args.types) {
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.android, getTypeForString(type), ScreenshotSize.androidPhone));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.android, getTypeForString(type), ScreenshotSize.android7Tablet));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.android, getTypeForString(type), ScreenshotSize.android10Tablet));
        generatedScreenshots.add(await createSingleScreenshotImage(
            args, name, screenshot, ScreenshotOSType.android, getTypeForString(type), ScreenshotSize.itchIo));
      }
    }
    return generatedScreenshots;
  }

  /// Creates a single processed screenshot with the provided settings.
  Future<File> createSingleScreenshotImage(ProcessScreenshotsArguments args, String name, ui.Image screenshot,
      ScreenshotOSType osType, ScreenshotType type, ScreenshotSize size) async {
    final screenshotImageSize = Size(screenshot.width.toDouble(), screenshot.height.toDouble());
    final screenshotSrc = Offset.zero & screenshotImageSize;

    final isLandScape = screenshotImageSize.width > screenshotSrc.height;

    var fileName = name;

    // Make the name early, so it's easy to sort
    switch (osType) {
      case ScreenshotOSType.android:
        fileName = fileName + "_android";
        break;
      case ScreenshotOSType.ios:
        fileName = fileName + "_ios";
        break;
    }
    switch (type) {
      case ScreenshotType.rescaled:
        fileName = fileName + "_rescaled";
        break;
      case ScreenshotType.promo:
        fileName = fileName + "_promo";
        break;
    }

    final recorder = ui.PictureRecorder();
    var width = 0.0;
    var height = 0.0;
    switch (size) {
      case ScreenshotSize.itchIo:
        width = 1920.0;
        height = 1080.0;
        fileName += "_itchio";
        break;
      case ScreenshotSize.androidPhone:
        width = 3840.0;
        height = 2160.0;
        fileName += "_phone";
        break;
      case ScreenshotSize.android7Tablet:
        // todo: this is rather 3:4 than 16:9
        width = 3840.0;
        height = 2880.0;
        fileName += "_tablet7";
        break;
      case ScreenshotSize.android10Tablet:
        // todo: this is rather 3:4 than 16:9
        width = 3840.0;
        height = 2880.0;
        fileName += "_tablet10";
        break;
      case ScreenshotSize.iPhone11ProMax:
        width = 2688.0;
        height = 1242.0;
        fileName += "_iphone_11_pro_max";
        break;
      case ScreenshotSize.iPhone11Pro:
        width = 2436.0;
        height = 1125.0;
        fileName += "_iphone_11_pro";
        break;
      case ScreenshotSize.iPhone6Plus:
        width = 2208.0;
        height = 1242.0;
        fileName += "_iphone_6_plus";
        break;
      case ScreenshotSize.iPhone6:
        width = 1334.0;
        height = 750.0;
        fileName += "_iphone_6";
        break;
      case ScreenshotSize.iPadPro3:
        width = 2048.0;
        height = 2732.0;
        fileName += "_iPad_3_pro";
        break;
    }

    // If it's in landscape, then change the orientations
    if (!isLandScape) {
      var cachedHeight = height;
      height = width;
      width = cachedHeight;
      fileName += "_portrait";
    } else {
      fileName += "_landscape";
    }

    final canvas = Canvas(recorder, Rect.fromPoints(const Offset(0.0, 0.0), Offset(width, height)));

    // Fill the background
    var parsedColor = ui.Color(int.parse(args.color, radix: 16));
    final backgroundPaint = Paint()
      ..shader = ui.Gradient.linear(
        const Offset(0, 0),
        Offset(0, height),
        [
          parsedColor,
          (args.useGradient)
              ? increaseColorLightness(increaseColorHue(increaseColorSaturation(parsedColor, 0.2), 20), -0.1)
              : parsedColor
        ],
      );
    canvas.drawRect(Rect.fromLTWH(0.0, 0.0, width, height), backgroundPaint);

    final fillPaint = Paint()
      ..color = ui.Color(int.parse(args.color, radix: 16))
      ..style = PaintingStyle.fill;

    // Drawing helper
    drawScreenshotToFullWidth() {
      var scalingFactor = screenshotImageSize.width / width;
      var overlayTargetSize = screenshotImageSize / scalingFactor;
      final dst = Offset.zero & overlayTargetSize;
      canvas.drawImageRect(screenshot, screenshotSrc, dst, fillPaint);
    }

    drawScreenshotToFullHeight() {
      var scalingFactor = screenshotImageSize.height / height;
      var overlayTargetSize = screenshotImageSize / scalingFactor;
      final dst = Offset.zero & overlayTargetSize;
      canvas.drawImageRect(screenshot, screenshotSrc, dst, fillPaint);
    }

    drawScreenshotToFitToHeight() {
      var scalingFactor = screenshotImageSize.height / height;
      var overlayTargetSize = screenshotImageSize / scalingFactor;
      final dst = Offset((width - overlayTargetSize.width) / 2, 0) & overlayTargetSize;
      canvas.drawImageRect(screenshot, screenshotSrc, dst, fillPaint);
    }

    drawScreenshotToFitToWidth() {
      var scalingFactor = screenshotImageSize.width / width;
      var overlayTargetSize = screenshotImageSize / scalingFactor;
      final dst = Offset(0, (height - overlayTargetSize.height) / 2) & overlayTargetSize;
      canvas.drawImageRect(screenshot, screenshotSrc, dst, fillPaint);
    }

    var ratioOfCanvas = width / height;
    var ratioOfScreenshot = screenshotImageSize.width / screenshotImageSize.height;
    drawScreenshot() {
      switch (type) {
        case ScreenshotType.rescaled:
          // Note: Rescaled images will be scaled to fill the complete screenshot.
          // Usually, they will be aligned at the top left
          if (isLandScape && ratioOfCanvas > ratioOfScreenshot) {
            drawScreenshotToFullWidth();
          } else if (isLandScape && ratioOfCanvas < ratioOfScreenshot) {
            drawScreenshotToFullHeight();
          } else if (ratioOfCanvas > ratioOfScreenshot) {
            drawScreenshotToFullWidth();
          } else if (ratioOfCanvas < ratioOfScreenshot) {
            drawScreenshotToFullHeight();
          } else {
            // Note: Expected ratio matches actual ratio
            drawScreenshotToFullWidth();
          }
          break;
        case ScreenshotType.promo:
          // Note: The promo screenshots will be fitted into the available space.
          // They will be centered and the background will be filled with the specified color.
          if (isLandScape && ratioOfCanvas > ratioOfScreenshot) {
            drawScreenshotToFitToHeight();
          } else if (isLandScape && ratioOfCanvas < ratioOfScreenshot) {
            drawScreenshotToFitToWidth();
          } else if (ratioOfCanvas > ratioOfScreenshot && (ratioOfCanvas - ratioOfScreenshot).abs() > 0.01) {
            drawScreenshotToFitToHeight();
          } else if (ratioOfCanvas < ratioOfScreenshot && (ratioOfCanvas - ratioOfScreenshot).abs() > 0.01) {
            drawScreenshotToFitToWidth();
          } else {
            // Note: Expected ratio matches actual ratio
            drawScreenshotToFitToWidth();
          }
          break;
      }
    }

    // Actually draw the screenshot
    drawScreenshot();

    // Generate the actual image
    final picture = recorder.endRecording();
    ui.Image img = await picture.toImage(width.toInt(), height.toInt());
    final pngBytes = await img.toByteData(format: ui.ImageByteFormat.png);
    if (pngBytes == null) throw ("PNG bytes should not be empty");

    // Save to persistent storage
    var file = await saveImageToFile(pngBytes.buffer.asUint8List(), fileName);
    return file;
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProcessScreenshotsArguments;
    createImages(args);

    return Scaffold(
      appBar: AppBar(
        //leading: Icon(Icons.menu),
        title: const Text("Processing"),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert))],
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CircularProgressIndicator(
            color: Colors.purpleAccent,
          ),
          SizedBox(
            height: 30,
          ),
          Text("This may take a while...")
        ],
      )),
    );
  }
}
