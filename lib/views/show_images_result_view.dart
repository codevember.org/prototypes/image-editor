import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/process.dart';
import 'package:cv_image_editor/views/create_images_view.dart';
import 'package:cv_image_editor/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

class ShowImagesResultView extends StatefulWidget {
  const ShowImagesResultView({Key? key}) : super(key: key);

  static const String route = '/result';

  @override
  State<ShowImagesResultView> createState() => _ShowImagesResultViewState();
}

class _ShowImagesResultViewState extends State<ShowImagesResultView> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProcessImageResultArguments;

    _dismissDialog() {
      Navigator.pop(context);
    }

    return Scaffold(
        appBar: AppBar(
          //leading: Icon(Icons.menu),
          title: const Text("Result"),
          actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert))],
        ),
        body: SafeArea(
          child: Scrollbar(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Images created for ${args.name}."),
                  const Divider(),
                  const Text("Android images"),
                  Container(
                    color: const Color.fromRGBO(0, 0, 0, 0.2),
                    margin: const EdgeInsets.only(top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Image.file(File(args.androidAppIcon), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.androidFeatureImage), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.androidTvBanner), fit: BoxFit.contain),
                        )
                      ],
                    ),
                  ),
                  const Divider(),
                  const Text("itch.io images"),
                  Container(
                    color: const Color.fromRGBO(0, 0, 0, 0.2),
                    margin: const EdgeInsets.only(top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Image.file(File(args.itchFavicon), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.itchCover), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.itchBanner), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.itchWideCover), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.itchSocial), fit: BoxFit.contain),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Image.file(File(args.itchLogo), fit: BoxFit.contain),
                        ),
                      ],
                    ),
                  ),
                  const Divider(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.purple),
                    onPressed: () async {
                      // Get the folder to save to
                      Directory? appDocDirectory = await getAppStorageDirectory();
                      if (appDocDirectory == null) return;

                      // Zip all the files
                      var encoder = ZipFileEncoder();
                      var zipFilePath = appDocDirectory.path + "/" + '${args.id}_${args.name}.zip';
                      encoder.create(zipFilePath);
                      encoder.addFile(File(args.appIconPath));
                      encoder.addFile(File(args.textLogoImagePath));

                      // Android
                      encoder.addFile(File(args.androidAppIcon));
                      encoder.addFile(File(args.androidFeatureImage));
                      encoder.addFile(File(args.androidTvBanner));

                      // itch.io
                      encoder.addFile(File(args.itchCover));
                      encoder.addFile(File(args.itchBanner));
                      encoder.addFile(File(args.itchSocial));
                      encoder.addFile(File(args.itchLogo));
                      encoder.close();

                      // Show the proceed dialog
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Export successful'),
                              content: const Text(".zip  file saved to internal storage!"),
                              actions: <Widget>[
                                SimpleDialogOption(
                                  onPressed: () {
                                    _dismissDialog();
                                  },
                                  child: const Text('Continue'),
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    Share.shareFiles([zipFilePath]);
                                    _dismissDialog();
                                  },
                                  child: const Text(
                                    'Share the .zip file',
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                ),
                              ],
                            );
                          });
                    },
                    child: const Text("Export as .zip"),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.grey),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, CreateImagesView.route);
                    },
                    child: const Text("Create another one"),
                  )
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: createBottomBar(context, (index) {
          setState(() {
            _selectedIndex = index;
          });
        }, _selectedIndex));
  }
}
