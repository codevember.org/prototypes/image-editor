import 'dart:io';

import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/process.dart';
import 'package:cv_image_editor/views/process_image_view.dart';
import 'package:cv_image_editor/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateImagesView extends StatefulWidget {
  const CreateImagesView({Key? key}) : super(key: key);

  static const String route = '/';

  @override
  State<CreateImagesView> createState() => _CreateImagesViewState();
}

class _CreateImagesViewState extends State<CreateImagesView> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: const CVImagePicker(),
        bottomNavigationBar: createBottomBar(context, (index) {
          setState(() {
            _selectedIndex = index;
          });
        }, _selectedIndex));
  }
}

class CVImagePicker extends StatefulWidget {
  const CVImagePicker({Key? key}) : super(key: key);

  @override
  _CVImagePickerState createState() => _CVImagePickerState();
}

class _CVImagePickerState extends State<CVImagePicker> {
  File? appIcon;
  File? textLogoImage;

  late TextEditingController _controller;
  late SharedPreferences _sharedPreferences;
  String? name;
  Color currentColor = Colors.white;
  Color pickerColor = Colors.white;
  bool shouldUseGradient = false;

  final String sharedPrefsWelcome = "welcome_screen";
  final String sharedPrefsImagesHowTo = "how_to_images";

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    SharedPreferences.getInstance().then((value) {
      _sharedPreferences = value;
      // Only show he help dialog the first time
      var welcomeShown = _sharedPreferences.getBool(sharedPrefsWelcome);
      if (welcomeShown == null) {
        showWelcomeDialog(context);
        _sharedPreferences.setBool(sharedPrefsWelcome, true);
      } else {
        var howToShown = _sharedPreferences.getBool(sharedPrefsImagesHowTo);
        if (howToShown == null) {
          showHowToDialog();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scrollbar(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: Text("Media images", style: Theme.of(context).textTheme.headline5),
                  ),
                  TextButton.icon(
                    onPressed: () {
                      showHowToDialog();
                    },
                    style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(Colors.purpleAccent),
                    ),
                    icon: const Icon(Icons.help_center),
                    label: const Text("How to"),
                  ),
                ],
              ),
              createHeading(context, "Get started", hideArrow: true),
              TextField(
                controller: _controller,
                onChanged: (String value) async {
                  setState(() {
                    name = value;
                  });
                },
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white, width: 2.0),
                  ),
                  labelStyle: TextStyle(color: Colors.white),
                  labelText: 'Enter a name',
                ),
              ),
              if (name == null || name?.isEmpty == true)
                const SizedBox(
                  height: 15,
                ),
              if (name == null || name?.isEmpty == true) createWarning("Enter at least one character as a name."),
              createHeading(context, "Select a background"),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(
                    child: Text(
                      "Background Color",
                      style: TextStyle(color: getFontColorForBackground(currentColor)),
                    ),
                    style: ElevatedButton.styleFrom(primary: currentColor),
                    onPressed: () {
                      // Show the proceed dialog
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              actions: <Widget>[
                                ColorPicker(
                                  pickerColor: currentColor,
                                  onColorChanged: (Color color) {
                                    setState(() => pickerColor = color);
                                  },
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    'Discard',
                                  ),
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    setState(() => currentColor = pickerColor);
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    'Set new color',
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                ),
                              ],
                            );
                          });
                    },
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.purpleAccent,
                    //fillColor: MaterialStateProperty.resolveWith(getColor),
                    value: shouldUseGradient,
                    onChanged: (bool? value) {
                      setState(() {
                        shouldUseGradient = value!;
                      });
                    },
                  ),
                  const Text("Use gradient")
                ],
              ),
              createHeading(context, "Add the image files"),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("App Icon"),
                      appIcon != null
                          ? Container(
                              color: const Color.fromRGBO(255, 255, 255, 0.2),
                              child: Image.file(
                                appIcon!,
                                width: 125,
                                height: 125,
                                fit: BoxFit.contain,
                              ),
                            )
                          : const Text(
                              'No image picked yet',
                              style: TextStyle(color: Colors.grey),
                            ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.purple),
                        onPressed: () {
                          pickImage((File file) {
                            setState(() {
                              appIcon = file;
                            });
                          });
                        },
                        child: Text(appIcon == null ? "Choose" : "Change"),
                      ),
                    ],
                  ),
                  const SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Text logo image"),
                      textLogoImage != null
                          ? Container(
                              color: const Color.fromRGBO(255, 255, 255, 0.2),
                              child: Image.file(
                                textLogoImage!,
                                width: 125,
                                height: 125,
                                fit: BoxFit.contain,
                              ),
                            )
                          : const Text(
                              'No image picked yet',
                              style: TextStyle(color: Colors.grey),
                            ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.purple),
                        onPressed: () {
                          pickImage((File file) {
                            setState(() {
                              textLogoImage = file;
                            });
                          });
                        },
                        child: Text(textLogoImage == null ? "Choose" : "Change"),
                      ),
                    ],
                  ),
                ],
              ),
              if (appIcon == null || textLogoImage == null) createWarning("Please select both images to continue"),
              arrowDown(),
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.purpleAccent),
                  onPressed: (name != null && name?.isNotEmpty == true && appIcon != null && textLogoImage != null)
                      ? () => Navigator.pushNamed(
                            context,
                            ProcessImagesView.route,
                            arguments: ProcessImagesArguments(
                              // A bit ugly, maybe there is a nicer way to write this
                              name: name!,
                              id: "image_" + DateTime.now().toIso8601String(),
                              // kind of hacky..,
                              color: currentColor.toString().split('(0x')[1].split(')')[0],
                              useGradient: shouldUseGradient,
                              appIconPath: appIcon!.path,
                              textLogoImagePath: textLogoImage!.path,
                            ),
                          )
                      : null,
                  child: const Text("Generate images"),
                ),
              ),
              const SizedBox(
                height: 45,
              )
            ],
          ),
        ),
      ),
    );
  }

  void showHowToDialog() {
    _sharedPreferences.setBool(sharedPrefsImagesHowTo, true);
    openFullScreenDialog(
        context,
        "How to",
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            createHeading(context, "Customize to your needs", hideArrow: true),
            const SizedBox(height: 30),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '1) Enter a '),
                  TextSpan(text: 'name', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(text: ' used for the generated files.'),
                ],
              ),
            ),
            const SizedBox(height: 30),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '2) Set a '),
                  TextSpan(
                      text: 'background color and choose the gradient flag',
                      style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(
                      text: '. This color will only be visible if you use an app icon with a transparent background.'),
                ],
              ),
            ),
            createHeading(context, "Add the graphics"),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '3a) Select an '),
                  TextSpan(text: 'app icon', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(
                      text: '. This should be square and for best results have a resolution of 1920x1920px. The '
                          'background can either be transparent or a solid color.'),
                ],
              ),
            ),
            const SizedBox(height: 15),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '3b) Select a '),
                  TextSpan(text: 'text logo image', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(
                      text: ': This should be your text on a transparent background. The This should be in landscape '
                          'format and for best results have a width resolution of 1920px.'),
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.purpleAccent),
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text("Got it!"),
            ),
            const SizedBox(
              height: 45,
            )
          ],
        ));
  }
}
