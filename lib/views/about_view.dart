import 'package:cv_image_editor/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutView extends StatefulWidget {
  const AboutView({Key? key}) : super(key: key);

  static const String route = '/about';

  @override
  State<AboutView> createState() => _AboutViewState();
}

class _AboutViewState extends State<AboutView> {
  int _selectedIndex = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Scrollbar(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 100),
                  Text("Awesome Thumbnail Composer",
                      style: Theme.of(context).textTheme.headline2),
                  const SizedBox(height: 100),
                  const Text("Created by Mathias Schlenker"),
                  const Text("As a part of Codevember.org and Visuv.io", style: TextStyle(color: Colors.grey)),
                  createHeading(context, "More info"),
                  const Text(
                      "This little tool creates a bunch of graphics which could be used for the Google PlayStore (Android), the AppStore (iOS/macOS) as well as websites such as itch.io."),
                  const SizedBox(height: 15),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.purple),
                      onPressed: () {
                        showWelcomeDialog(context);
                      },
                      child: const Text("Re-open the welcome dialog")),
                  const SizedBox(height:30,),
                  createHeading(context, "Privacy"),
                  const Text("We're not collecting any data and therefore we aren't sending any data 'home'. If your interested in our motivation and our backstory, check out our homepage:"),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.purple),
                      onPressed: () async {
                        var _url = "https://codevember.org/";
                        if (!await launch(_url)) throw 'Could not launch $_url';
                      },
                      child: const Text("Open Codevember.org")),
                  const SizedBox(height:30,),
                  createHeading(context, "About"),
                  const Text("This project is open source. If there is anything missing or if you have some feature-requests, just get in touch with us: codevember.team@gmail.com"),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.purple),
                      onPressed: () async {
                        var _url = "https://gitlab.com/codevember.org/prototypes/image-editor";
                        if (!await launch(_url)) throw 'Could not launch $_url';
                      },
                      child: const Text("Open GitLab project")),
                  const SizedBox(height: 45,)
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: createBottomBar(context, (index) {
          setState(() {
            _selectedIndex = index;
          });
        }, _selectedIndex));
  }
}
