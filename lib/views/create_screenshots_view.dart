import 'dart:io';

import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/process.dart';
import 'package:cv_image_editor/views/process_screenshots_view.dart';
import 'package:cv_image_editor/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import 'package:shared_preferences/shared_preferences.dart';

class CreateScreenshotsView extends StatefulWidget {
  const CreateScreenshotsView({Key? key}) : super(key: key);

  static const String route = '/screenshots';

  @override
  State<CreateScreenshotsView> createState() => _CreateScreenshotsViewState();
}

class _CreateScreenshotsViewState extends State<CreateScreenshotsView> {
  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: const CVScreenshotPicker(),
        bottomNavigationBar: createBottomBar(context, (index) {
          setState(() {
            _selectedIndex = index;
          });
        }, _selectedIndex));
  }
}

class CVScreenshotPicker extends StatefulWidget {
  const CVScreenshotPicker({Key? key}) : super(key: key);

  @override
  _CVScreenshotPickerState createState() => _CVScreenshotPickerState();
}

// TODO: move this into the class as a static?
final initialOSItems = [
  MultiSelectItem(ScreenshotOSType.android.name, "Android"),
  MultiSelectItem(ScreenshotOSType.ios.name, "iOS"),
].toList();

final initialTypeItems = [
  MultiSelectItem(ScreenshotType.rescaled.name, "Rescaled"),
  MultiSelectItem(ScreenshotType.promo.name, "Promo"),
].toList();

class _CVScreenshotPickerState extends State<CVScreenshotPicker> {
  List<File> files = [];

  late TextEditingController _controller;
  late SharedPreferences _sharedPreferences;

  String? name;
  Color currentColor = Colors.white;
  Color pickerColor = Colors.white;
  bool shouldUseGradient = false;

  List<MultiSelectItem> selectedOSItems = initialOSItems;
  List<MultiSelectItem> selectedTypeItems = initialTypeItems;

  List<String> selectedOSItemValues = initialOSItems.map((e) => e.value).toList();
  List<String> selectedTypeItemValues = initialTypeItems.map((e) => e.value).toList();

  final String sharedPrefsScreenshotsHowTo = "how_to_screenshots";

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    SharedPreferences.getInstance().then((value) {
      _sharedPreferences = value;
      // Only show he help dialog the first time
      var wasAlreadyShown = _sharedPreferences.getBool(sharedPrefsScreenshotsHowTo);
      if (wasAlreadyShown == null) {
        showHowToDialog();
        _sharedPreferences.setBool(sharedPrefsScreenshotsHowTo, true);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scrollbar(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: Text("Screenshots", style: Theme.of(context).textTheme.headline5),
                  ),
                  TextButton.icon(
                    onPressed: () => {showHowToDialog()},
                    style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(Colors.purpleAccent),
                    ),
                    icon: const Icon(Icons.help_center),
                    label: const Text("How to"),
                  ),
                ],
              ),
              createHeading(context, "Get started", hideArrow: true),
              TextField(
                controller: _controller,
                onChanged: (String value) async {
                  setState(() {
                    name = value;
                  });
                },
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white, width: 2.0),
                  ),
                  labelStyle: TextStyle(color: Colors.white),
                  labelText: 'Choose a name',
                ),
              ),
              if (name == null || name?.isEmpty == true) const SizedBox(height: 15),
              if (name == null || name?.isEmpty == true) createWarning("Enter at least one character as a name."),
              createHeading(context, "Choose the background"),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(
                    child: Text(
                      "Background Color",
                      style: TextStyle(color: getFontColorForBackground(currentColor)),
                    ),
                    style: ElevatedButton.styleFrom(primary: currentColor),
                    onPressed: () {
                      // Show the proceed dialog
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              actions: <Widget>[
                                ColorPicker(
                                  pickerColor: currentColor,
                                  onColorChanged: (Color color) {
                                    setState(() => pickerColor = color);
                                  },
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    'Discard',
                                  ),
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    setState(() => currentColor = pickerColor);
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    'Set new color',
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                ),
                              ],
                            );
                          });
                    },
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.purpleAccent,
                    //fillColor: MaterialStateProperty.resolveWith(getColor),
                    value: shouldUseGradient,
                    onChanged: (bool? value) {
                      setState(() {
                        shouldUseGradient = value!;
                      });
                    },
                  ),
                  const Text("Use gradient")
                ],
              ),
              createHeading(context, "Select types to create"),
              Row(
                children: [
                  const Text("OS"),
                  Expanded(
                    child: MultiSelectChipField(
                      showHeader: false,
                      items: selectedOSItems,
                      decoration: const BoxDecoration(),
                      selectedTextStyle: const TextStyle(color: Colors.white),
                      textStyle: const TextStyle(color: Colors.black),
                      icon: const Icon(Icons.check),
                      onTap: (values) {
                        setState(() {
                          selectedOSItemValues = values.cast<String>();
                        });
                      },
                      initialValue: selectedOSItemValues,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const Text("Type"),
                  Expanded(
                    child: MultiSelectChipField(
                      showHeader: false,
                      items: selectedTypeItems,
                      decoration: const BoxDecoration(),
                      selectedTextStyle: const TextStyle(color: Colors.white),
                      textStyle: const TextStyle(color: Colors.black),
                      icon: const Icon(Icons.check),
                      onTap: (values) {
                        setState(() {
                          selectedTypeItemValues = values.cast<String>();
                        });
                      },
                      initialValue: selectedTypeItemValues,
                    ),
                  ),
                ],
              ),
              if (selectedOSItemValues.isEmpty || selectedTypeItemValues.isEmpty)
                createWarning("Make sure to select at least one operating system and one configuration type."),
              createHeading(context, "Add screenshots"),
              if (files.isNotEmpty)
                GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 3,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: <Widget>[
                    for (var item in files) Image.file(item, width: 125, height: 125, fit: BoxFit.contain)
                  ],
                ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Colors.purple),
                onPressed: () {
                  pickImage((File file) {
                    setState(() {
                      files.add(file);
                    });
                  });
                },
                child: const Text("Add screenshot"),
              ),
              const SizedBox(height: 30),
              if (files.isEmpty) createWarning("Please select at least one screenshot to continue"),
              arrowDown(),
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.purpleAccent),
                  onPressed: (name != null && name?.isNotEmpty == true && files.isNotEmpty)
                      ? () {
                          Navigator.pushNamed(
                            context,
                            ProcessScreenshotsView.route,
                            arguments: ProcessScreenshotsArguments(
                              name: name!,
                              // A bit ugly, maybe there is a nicer way to write this
                              id: "image_" + DateTime.now().toIso8601String(),
                              color: currentColor.toString().split('(0x')[1].split(')')[0],
                              // kind of hacky..,
                              osTypes: selectedOSItemValues,
                              types: selectedTypeItemValues,
                              useGradient: shouldUseGradient,
                              screenshotPaths: files.map((f) => f.path).toList(),
                            ),
                          );
                        }
                      : null,
                  child: const Text("Generate images"),
                ),
              ),
              const SizedBox(
                height: 45,
              )
            ],
          ),
        ),
      ),
    );
  }

  void showHowToDialog() {
    openFullScreenDialog(
        context,
        "How to",
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            createHeading(context, "Customize to your needs", hideArrow: true),
            const SizedBox(height: 30),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '1) Enter a '),
                  TextSpan(text: 'name', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(text: ' used for the generated files.'),
                ],
              ),
            ),
            const SizedBox(height: 30),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '2) Set a '),
                  TextSpan(
                      text: 'background color and choose the gradient flag',
                      style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(
                      text: '. This color will only be visible if you use an app icon with a transparent background.'),
                ],
              ),
            ),
            createHeading(context, "Set the types to create"),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '3a) Select the '),
                  TextSpan(text: 'operating systems', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(
                      text: ' you want to create your screenshots for. The logic here is heavily aligned with the '
                          'requirements by the PlayStore and the AppStore.'),
                ],
              ),
            ),
            const SizedBox(height: 30),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '3b) Select the '),
                  TextSpan(text: 'types', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(text: ' of screenshots you want to create: \n'),
                  TextSpan(text: 'Rescaled', style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(
                      text: ' means the imported screenshots will be scaled to align to the needed resolutions. \n'),
                  TextSpan(text: 'Promo', style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(
                      text: ' means the imported screenshots will be scaled to fit into the needed resolutions - the '
                          'rest is filled with the specified background color(s). '),
                ],
              ),
            ),
            createHeading(context, "Add the graphics"),
            const Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(text: '4) Add a bunch of '),
                  TextSpan(text: 'screenshots', style: TextStyle(color: Colors.purpleAccent)),
                  TextSpan(
                      text: ': The screenshots can be either in landscape or portrait and will be fitted to the needed '
                          'resolutions. \n'),
                  TextSpan(text: 'Note:', style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(
                      text: 'Since we\'re using pretty basic rescaling here, be aware that the screenshots can be cut '
                          'of if the ratio differs a lot (3:4 vs ultra-wide). You may want to upload various screenshots with '
                          'various ratios and review the generated files afterwards for the best results.'),
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.purpleAccent),
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text("Got it!"),
            ),
            const SizedBox(
              height: 45,
            )
          ],
        ));
  }

}
