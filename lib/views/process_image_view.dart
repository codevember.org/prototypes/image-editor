import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/process.dart';
import 'package:cv_image_editor/views/show_images_result_view.dart';
import 'package:flutter/material.dart';

class ProcessImagesView extends StatefulWidget {
  const ProcessImagesView({Key? key}) : super(key: key);

  static const String route = '/processImages';

  @override
  _ProcessImagesViewState createState() => _ProcessImagesViewState();
}

class _ProcessImagesViewState extends State<ProcessImagesView> {
  Future<void> createImages(ProcessImagesArguments args) async {
    // Note: Maybe this could be a simple array
    final androidAppIcon = await createImage(args, ImageType.androidAppIcon);
    final androidFeatureImage = await createImage(args, ImageType.androidFeatureImage);
    final androidTvBanner = await createImage(args, ImageType.androidTvBanner);

    final itchCover = await createImage(args, ImageType.itchCover);
    final itchBanner = await createImage(args, ImageType.itchBanner);
    final itchFavicon = await createImage(args, ImageType.itchFavicon);
    final itchWideCover = await createImage(args, ImageType.itchWideCover);
    final itchSocial = await createImage(args, ImageType.itchSocial);
    final itchLogo = await createImage(args, ImageType.itchLogo);

    // For now just wait at least 500ms
    Future.delayed(const Duration(milliseconds: 100), () {
      Navigator.popAndPushNamed(
        context,
        ShowImagesResultView.route,
        arguments: ProcessImageResultArguments(
            name: args.name,
            id: args.id,
            appIconPath: args.appIconPath,
            textLogoImagePath: args.textLogoImagePath,
            androidAppIcon: androidAppIcon.path,
            androidFeatureImage: androidFeatureImage.path,
            androidTvBanner: androidTvBanner.path,
            itchCover: itchCover.path,
            itchBanner: itchBanner.path,
            itchFavicon: itchFavicon.path,
            itchWideCover: itchWideCover.path,
            itchSocial: itchSocial.path,
            itchLogo: itchLogo.path),
      );
    });
  }

  /// Creats a single image for the type provided.
  Future<File> createImage(ProcessImagesArguments args, ImageType type) async {
    var appIcon = await loadImage(File(args.appIconPath));
    final appIconImageSize = Size(appIcon.width.toDouble(), appIcon.height.toDouble());
    final appIconSrc = Offset.zero & appIconImageSize;

    var textLogo = await loadImage(File(args.textLogoImagePath));
    final textLogoImageSize = Size(textLogo.width.toDouble(), textLogo.height.toDouble());
    final textLogoSrc = Offset.zero & textLogoImageSize;

    final recorder = ui.PictureRecorder();
    var width = 0.0;
    var height = 0.0;
    switch (type) {
      case ImageType.androidAppIcon:
        width = 512.0;
        height = 512.0;
        break;
      case ImageType.androidFeatureImage:
        width = 1024.0;
        height = 500.0;
        break;
      case ImageType.androidTvBanner:
        width = 1280.0;
        height = 720.0;
        break;
      case ImageType.itchCover:
        width = 630.0;
        height = 500.0;
        break;
      case ImageType.itchBanner:
        width = 960.0;
        height = 400.0;
        break;
      case ImageType.itchFavicon:
        width = 256.0;
        height = 256.0;
        break;
      case ImageType.itchWideCover:
        width = 1600.0;
        height = 686.0;
        break;
      case ImageType.itchSocial:
        width = 1200.0;
        height = 630.0;
        break;
      case ImageType.itchLogo:
        width = 1920.0;
        height = 1080.0;
        break;
    }

    final canvas = Canvas(recorder, Rect.fromPoints(const Offset(0.0, 0.0), Offset(width, height)));

    // Fill the background
    var parsedColor = ui.Color(int.parse(args.color, radix: 16));
    final backgroundPaint = Paint()
      ..shader = ui.Gradient.linear(
        const Offset(0, 0),
        Offset(0, height),
        [
          parsedColor,
          (args.useGradient)
              ? increaseColorLightness(increaseColorHue(increaseColorSaturation(parsedColor, 0.2), 20), -0.1)
              : parsedColor
        ],
      );
    if (type != ImageType.itchLogo) {
      canvas.drawRect(Rect.fromLTWH(0.0, 0.0, width, height), backgroundPaint);
    }

    final fillPaint = Paint()
      ..color = ui.Color(int.parse(args.color, radix: 16))
      ..style = PaintingStyle.fill;

    // Drawing helper
    drawIconIntoBackground() {
      var iconTargetSize = Size(width, width);
      final iconDst = Offset.zero & iconTargetSize;
      canvas.drawImageRect(appIcon, appIconSrc, iconDst, fillPaint);
    }

    drawLogoTextFullWidth() {
      // Draw the text appIcon image on top
      var scalingFactor = textLogoImageSize.width / (width / 1);
      var overlayTargetSize = textLogoImageSize / scalingFactor;
      final dst = Offset.zero & overlayTargetSize;
      canvas.drawImageRect(textLogo, textLogoSrc, dst, fillPaint);
    }

    drawDefaultLayout() {
      drawIconIntoBackground();
      drawLogoTextFullWidth();
    }

    var fileName = args.name;
    // just draw: canvas.drawImage(appIcon, Offset(textLogo.width.toDouble(), 0), fillPaint);
    switch (type) {
      case ImageType.androidAppIcon:
        var targetSize = Size(width, height);
        final dst = Offset.zero & targetSize;
        canvas.drawImageRect(appIcon, appIconSrc, dst, fillPaint);
        fileName = fileName + "_androidAppIcon";
        break;
      case ImageType.androidFeatureImage:
        drawDefaultLayout();
        fileName = fileName + "_androidFeatureImage";
        break;
      case ImageType.androidTvBanner:
        drawDefaultLayout();
        fileName = fileName + "_androidTvBanner";
        break;
      case ImageType.itchCover:
        drawIconIntoBackground();
        // Draw the text appIcon image on top
        var scalingFactor = textLogoImageSize.width / (width / 1);
        var overlayTargetSize = textLogoImageSize / scalingFactor;
        final dst = Offset(0, height - overlayTargetSize.height) & overlayTargetSize;
        canvas.drawImageRect(textLogo, textLogoSrc, dst, fillPaint);
        fileName = fileName + "_itchCover";
        break;
      case ImageType.itchBanner:
        drawDefaultLayout();
        fileName = fileName + "_itchBanner";
        break;
      case ImageType.itchFavicon:
        var iconTargetSize = Size(width, width);
        final iconDst = Offset.zero & iconTargetSize;
        canvas.drawImageRect(appIcon, appIconSrc, iconDst, fillPaint);

        fileName = fileName + "_itchFavicon";
        break;
      case ImageType.itchWideCover:
        drawDefaultLayout();
        fileName = fileName + "_itchBanner";
        break;
      case ImageType.itchSocial:
        drawDefaultLayout();
        fileName = fileName + "_itchSocial";
        break;
      case ImageType.itchLogo:
        drawLogoTextFullWidth();
        fileName = fileName + "_itchLogo";
        break;
    }

    // Generate the actual image
    final picture = recorder.endRecording();
    ui.Image img = await picture.toImage(width.toInt(), height.toInt());
    final pngBytes = await img.toByteData(format: ui.ImageByteFormat.png);
    if (pngBytes == null) throw ("PNG bytes should not be empty");

    // Save to persistent storage
    var file = await saveImageToFile(pngBytes.buffer.asUint8List(), fileName);
    return file;
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProcessImagesArguments;
    createImages(args);

    return Scaffold(
      appBar: AppBar(
        //leading: Icon(Icons.menu),
        title: const Text("Processing"),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert))],
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CircularProgressIndicator(
            color: Colors.purpleAccent,
          ),
          SizedBox(height: 30,),
          Text("This may take a while...")
        ],
      )),
    );
  }
}
