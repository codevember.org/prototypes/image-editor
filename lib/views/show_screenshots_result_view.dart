import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/process.dart';
import 'package:cv_image_editor/views/create_images_view.dart';
import 'package:cv_image_editor/views/create_screenshots_view.dart';
import 'package:cv_image_editor/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

class ShowScreenshotsResultView extends StatefulWidget {
  const ShowScreenshotsResultView({Key? key}) : super(key: key);

  static const String route = '/screenshotResult';

  @override
  State<ShowScreenshotsResultView> createState() => _ShowScreenshotsResultViewState();
}

class _ShowScreenshotsResultViewState extends State<ShowScreenshotsResultView> {
  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProcessScreenshotResultArguments;

    _dismissDialog() {
      Navigator.pop(context);
    }

    return Scaffold(
        appBar: AppBar(
          //leading: Icon(Icons.menu),
          title: const Text("Result"),
          actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert))],
        ),
        body: Container(
          margin: const EdgeInsets.all(16.0),
          child: Scrollbar(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text("Images created for ${args.name}."),
                  const Divider(),
                  const Text("Android images"),
                  Container(
                    color: const Color.fromRGBO(0, 0, 0, 0.2),
                    margin: const EdgeInsets.only(top: 10.0),
                    child: GridView.count(
                      primary: false,
                      padding: const EdgeInsets.all(20),
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 3,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      children: <Widget>[
                        for (var path in args.screenshotPaths.where((element) => element.contains("android")))
                          Image.file(File(path), width: 125, height: 125, fit: BoxFit.contain)
                      ],
                    ),
                  ),
                  const Divider(),
                  const Text("iOS images"),
                  Container(
                    color: const Color.fromRGBO(0, 0, 0, 0.2),
                    margin: const EdgeInsets.only(top: 10.0),
                    child: GridView.count(
                      primary: false,
                      padding: const EdgeInsets.all(20),
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 3,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      children: <Widget>[
                        for (var path in args.screenshotPaths.where((element) => element.contains("ios")))
                          Image.file(File(path), width: 125, height: 125, fit: BoxFit.contain)
                      ],
                    ),
                  ),
                  const Divider(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.purple),
                    onPressed: () async {
                      // Get the folder to save to
                      Directory? appDocDirectory = await getAppStorageDirectory();
                      if (appDocDirectory == null) return;

                      // Zip all the files
                      var encoder = ZipFileEncoder();
                      var zipFilePath = appDocDirectory.path + "/screenshots-" + '${args.id}_${args.name}.zip';
                      encoder.create(zipFilePath);

                      for (var element in args.screenshotPaths) {
                        encoder.addFile(File(element));
                        print(element);
                      }
                      encoder.close();

                      // Show the proceed dialog
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Export successful'),
                              content: const Text(".zip  file saved to internal storage!"),
                              actions: <Widget>[
                                SimpleDialogOption(
                                  onPressed: () {
                                    _dismissDialog();
                                  },
                                  child: const Text('Continue'),
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    Share.shareFiles([zipFilePath]);
                                    _dismissDialog();
                                  },
                                  child: const Text(
                                    'Share the .zip file',
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                ),
                              ],
                            );
                          });
                    },
                    child: const Text("Export as .zip"),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.grey),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, CreateScreenshotsView.route);
                    },
                    child: const Text("Create another one"),
                  ),
                  const SizedBox(height: 45,)
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: createBottomBar(context, (index) {
          setState(() {
            _selectedIndex = index;
          });
        }, _selectedIndex));
  }
}
