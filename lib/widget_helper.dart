import 'package:cv_image_editor/helper.dart';
import 'package:cv_image_editor/views/about_view.dart';
import 'package:cv_image_editor/views/create_images_view.dart';
import 'package:cv_image_editor/views/create_screenshots_view.dart';
import 'package:flutter/material.dart';

BottomNavigationBar createBottomBar(BuildContext context, Function(int) callback, int currentIndex) {
  return BottomNavigationBar(
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.perm_media),
        label: 'Media images',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.screenshot),
        label: 'Screenshots',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.info),
        label: 'About',
      ),
    ],
    currentIndex: currentIndex,
    selectedItemColor: Colors.purpleAccent,
    onTap: (index) {
      callback(index);

      switch (index) {
        case 0:
          navigateToViewWithoutTransition(context, const CreateImagesView());
          break;
        case 1:
          navigateToViewWithoutTransition(context, const CreateScreenshotsView());
          break;
        case 2:
          navigateToViewWithoutTransition(context, const AboutView());
          break;
      }
    },
  );
}

void openFullScreenDialog(BuildContext context, String title, Widget content) {
  Navigator.push(
    context,
    MaterialPageRoute<void>(
      builder: (BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SafeArea(
          child: Scrollbar(child: SingleChildScrollView(padding: const EdgeInsets.all(8.0), child: content)),
        ),
      ),
      fullscreenDialog: true,
    ),
  );
}

Widget createHeading(BuildContext context, String title, {bool hideArrow = false}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const SizedBox(height: 45),
      if (!hideArrow)
        const Center(
            child: Icon(
          Icons.arrow_downward,
          size: 40,
        )),
      if (!hideArrow) const SizedBox(height: 45),
      Text(title, style: Theme.of(context).textTheme.headline6),
      const SizedBox(height: 15),
    ],
  );
}

Widget arrowDown() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: const [
      SizedBox(height: 15),
      Center(
          child: Icon(
        Icons.arrow_downward,
        size: 40,
      )),
      SizedBox(height: 15),
    ],
  );
}

Text createWarning(String text) {
  return Text(text, style: const TextStyle(color: Colors.amber));
}

void showWelcomeDialog(BuildContext context) {
  openFullScreenDialog(
      context,
      "Welcome",
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("You're probably wondering why you landed here. Let's explain the app's basics."),
          createHeading(context, "What?", hideArrow: true),
          const Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: 'Awesome Thumbnail Composer is a simple app that allows you to create a bunch of graphics which '
                        'could be used for the '),
                TextSpan(
                    text: 'Google PlayStore (Android), the AppStore (iOS/macOS) ',
                    style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(text: 'as well as websites such as '),
                TextSpan(text: 'itch.io', style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(text: '. We basically provide two image generators:'),
              ],
            ),
          ),
          createHeading(context, "Image generation"),
          const Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(text: 'Image generation creates a bunch of media images. For this you can upload the '),
                TextSpan(text: 'app icon', style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(text: ' as well as a '),
                TextSpan(text: 'transparent text icon', style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(text: '. The generated images are basically the social,featured and marketing images.'),
              ],
            ),
          ),
          createHeading(context, "Screenshot generation"),
          const Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: 'Screenshot generation creates various formats of the screenshots you\'ve uploaded. '
                        'You can choose if the screenshots should only be '),
                TextSpan(text: 'rescaled', style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(
                    text: ' to the desired target resolutions or if they should be fit inside the available '
                        'space and the background should be filled ('),
                TextSpan(text: 'Promo', style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(text: ').'),
              ],
            ),
          ),
          createHeading(context, "Lost or need help?"),
          const Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(text: 'Just look out for the '),
                TextSpan(text: '"How to"', style: TextStyle(color: Colors.purpleAccent)),
                TextSpan(text: ' button at the top right, it will open a help overlay with all you need.'),
              ],
            ),
          ),
          createHeading(context, "Saving and exporting"),
          const Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: 'After the images and screenshots are created, you can easily save, zip and share '
                        'your files. Play around with the settings and try different formats!'),
              ],
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.purpleAccent),
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("Go get started!"),
          ),
          const SizedBox(
            height: 45,
          )
        ],
      ));
}
