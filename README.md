# Awesome Thumbnail Composer

A Flutter based project, which implements a basic image generator for all social and showcase screenshots and images.

## In more detail

Awesome Thumbnail Composer is a simple app that allows you to create graphics optimized in Aspect Ratios for the Google Play Store (Android), the App Store (iOS/macOS) as well as websites such as itch.io.
We provide two image generators: Image generation creates a bunch of media images. For this you can upload the app icon as well as a transparent text icon. The generated images are represent the social, featured and marketing images needed for store purposes.
Screenshot generation creates various formats of the screenshots you've uploaded. You can choose if the screenshots should only be rescaled to the desired target resolutions or if they should be fit inside the available space and the background should be filled (Promo).
After the images and screenshots are created, you can easily save, zip and share your files. Play around with the settings and try different formats!

## Roadmap

- [x] Load and create media images
- [x] Load and create screenshots
- [x] Basic build for Android and iOS
- [x] Fastlane support
- [ ] MacOS and Windows builds
- [ ] More formats
- [ ] More layout options
- [ ] Hints for wrong formats

## Implemented by

This project was created by Mathias Schlenker and can be used as a Codevember.org prototype.

## Participate

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
